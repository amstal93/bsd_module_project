<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| billing\_id | n/a | `any` | n/a | yes |
| folder\_id | The folder to deploy in | `any` | n/a | yes |
| org\_id | The numeric organization id | `any` | n/a | yes |
| project\_id | the name of the pre-made project the build will run in | `any` | n/a | yes |
| vpc\_host | The name of vpc host project | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| project\_id | n/a |
| sa\_key | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->